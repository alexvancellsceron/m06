<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empreses extends Model
{
    use HasFactory;

    protected $table = 'Empreses';
    protected $primaryKey = 'IDEmpresa';
    protected $fillable=['Nom', 'Adreça', 'Telefon', 'Mail'];

    public function IDOfertes()
    {
        return $this -> hasMany(Ofertes::class);
    }
}
