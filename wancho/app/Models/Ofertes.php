<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ofertes extends Model
{
    use HasFactory;

    protected $table = 'Ofertes';
    protected $primaryKey = 'IDOferta';
    protected $fillable=['Descripcio', 'Vacants', 'NomContacte', 'CognomContacte', 'MailContacte', 'Curs', 'IDEmpresa'];

    public function IDEmpresa()
    {
        return $this -> belongsTo(Empreses::class);
    }

    public function IDEstudis()
    {
        return $this -> belongsToMany(Estudis::class, 'ofertes_estudis', 'IDOferta', 'IDEstudi')->withTimestamps();
    }

    public function IDAlumnes()
    {
        return $this -> belongsToMany(Estudis::class, 'enviaments', 'IDOferta', 'IDAlumnes')->withTimestamps();
    }

}
