<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alumnes extends Model
{
    use HasFactory;

    protected $table = 'Alumnes';
    protected $primaryKey = 'IDAlumnes';
    protected $fillable=['Nom', 'Cognom', 'DNI', 'Curs', 'Cicle', 'Telefon', 'Mail', 'Practiques', 'CV', 'IDOferta', 'IDUser'];

    public function IDUser()
    {
        return $this->belongsTo(User::class);
    }

    public function IDOfertes()
    {
        return $this -> belongsToMany(Ofertes::class, 'enviaments', 'IDAlumnes', 'IDOferta')->withTimestamps();
    }


}
