<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estudis extends Model
{
    use HasFactory;

    protected $table = 'Estudis';
    protected $primaryKey = 'IDEstudi';
    protected $fillable=['IDEstudi', 'NomCicle'];
    //'SMX', 'DAMVI', 'DAM', 'ASIX'
    public function IDOfertes()
    {
        return $this -> belongsToMany(Estudis::class, 'ofertes_estudis', 'IDEstudi', 'IDOferta')->withTimestamps();
    }
}
