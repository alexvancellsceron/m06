<?php

namespace App\Http\Controllers;

use App\Models\Empreses;
use App\Models\Ofertes;
use App\Models\Estudis;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CoordinadorFCTController extends Controller
{
    public function addEmpresa(Request $Request)
    {
        $event = new Empreses();
        $event->Nom=$Request->nom;
        $event->Adreça=$Request->adreca;
        $event->Telefon=$Request->telefon;
        $event->Mail=$Request->mail;

        $event->save();

        return redirect('/empreses')->with('success','Empresa creada amb èxit!.');
    }

    public function editEmpresa(Request $Request)
    {
        $event = Empreses::find($Request->id);
        $event->Nom=$Request->nom;
        $event->Adreça=$Request->adreca;
        $event->Telefon=$Request->telefon;
        $event->Mail=$Request->mail;

        $event->save();

        return redirect('/empreses')->with('success','Empresa actualitzada amb èxit!.');
    }

    public function getFormulariEmpreses(){
        return view('add_empresa');
    }

    public function getFormulariEmpresesEditar($id){
        $empresa = Empreses::findOrFail($id);
        //return $tots->toJson();
        return view('edit_empresa', [
            'empresa' => $empresa
        ]);
    }

    public function getFormulariOfertes($id){
        $empresa = Empreses::find($id);
        $user = User::findOrFail(Auth::user()->getAuthIdentifier());
        $ofertes = DB::table('ofertes')->where('IDEmpresa', $id)->get();
        return view('ofertes_empresa', [
            'empresa' => $empresa,
            'ofertes' => $ofertes,
            "user" => $user
        ]);

    }

    public function getFormulariOfertesEditar($id)
    {
        $cicles = DB::table('estudis')->get();
        $oferta = DB::table('ofertes')->where('IDOferta', $id)->first();
        return view('edit_oferta', [
            'oferta' => $oferta,
            'cicles' => $cicles
        ]);
    }

    public function actualitzaOferta(Request $Request)
    {
        $event = Ofertes::find($Request->id);
        $event->Descripcio=$Request->descripcio;
        $event->Vacants=$Request->vacants;
        $event->NomContacte=$Request->nomcontacte;
        $event->CognomContacte=$Request->cognomcontacte;
        $event->MailContacte=$Request->mail;
        $event->Curs=$Request->curs;

        $event->save();

        return redirect('/empreses')->with('success','Oferta actualitzada amb èxit!.');
    }

    public function getFormulariOfertesAfegir($id)
    {
        $cicles = DB::table('estudis')->get();
        $empresa = DB::table('empreses')->where('IDEmpresa', $id)->first();
        return view('add_oferta', [
            'empresa' => $empresa,
            'cicles' => $cicles
        ]);
    }


    public function insertOferta(Request $Request)
    {
        $event = new Ofertes();
        $event->Descripcio=$Request->descripcio;
        $event->Vacants=$Request->vacants;
        $event->NomContacte=$Request->nomcontacte;
        $event->CognomContacte=$Request->cognomcontacte;
        $event->MailContacte=$Request->mail;
        $event->Curs=$Request->curs;
        $event->IDEmpresa=$Request->id;

        $event->save();

        return redirect('/empreses')->with('success','Oferta creada amb èxit!.');
    }




}
