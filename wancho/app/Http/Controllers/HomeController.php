<?php

namespace App\Http\Controllers;

use App\Models\Empreses;
use App\Models\Alumnes;
use App\Models\Ofertes;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard. ^^
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('dashboard');
    }

    public function getEmpreses(){
        $user = User::findOrFail(Auth::user()->getAuthIdentifier());
        $empreses = Empreses::paginate(50);
        //return $tots->toJson();
        return view('empreses', [
            'user' => $user,
            'empreses' => $empreses
        ]);
    }

    public function getAlumnes(){
        $user = User::findOrFail(Auth::user()->getAuthIdentifier());
        $alumnes = Alumnes::paginate(50);
        $tutors = DB::table("users")->get();
        //return $tots->toJson();
        return view('alumnes', [
            'user' => $user,
            'alumnes' => $alumnes,
            'tutors' => $tutors
        ]);
    }

    public function getOfertes(){
        $user = User::findOrFail(Auth::user()->getAuthIdentifier());
        $ofertes = Ofertes::paginate(50);
        //return $tots->toJson();
        return view('ofertes', [
            'user' => $user,
            'ofertes' => $ofertes
        ]);
    }
}
