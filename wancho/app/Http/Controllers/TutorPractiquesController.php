<?php

namespace App\Http\Controllers;

use App\Models\Alumnes;
use App\Models\Estudis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TutorPractiquesController extends Controller
{

    public function getFormulariAlumnesAfegir()
    {
        $cicles = DB::table('estudis')->get();
        $tutors = DB::table('users')->get();
        return view('add_alumne',[
            "cicles" => $cicles,
            "tutors" => $tutors
        ]);
    }
    public function getFormulariAlumnesEditar($id)
    {
        $alumne = Alumnes::findOrFail($id);
        $cicles = DB::table('estudis')->get();
        $tutors = DB::table('users')->get();
        //return $tots->toJson();
        return view('edit_alumne', [
            'alumne' => $alumne,
            'cicles' => $cicles,
            'tutors' => $tutors
        ]);
    }

    public function editAlumne(Request $Request)
    {
        $event = Alumnes::find($Request->id);
        $event->Nom=$Request->Nom;
        $event->Cognom=$Request->Cognom;
        $event->Curs=$Request->Curs;
        $event->Mail=$Request->Mail;
        if($Request->Practiques=="on")
            $event->Practiques = true;
        else
            $event->Practiques = false;
        $event->Telefon=$Request->Telefon;
        $event->CV=$Request->CV;
        $event->DNI=$Request->DNI;
        $event->IDUser=$Request->IDUser;

        $event->save();

        return redirect('/alumnes')->with('success','Alumne actualitzat amb èxit!.');
    }

    public function insertAlumne(Request $Request)
    {
        //$file = $Request->file("CV");
        //$file = file_get_contents($file);
        //dd($file);
        $event = new Alumnes();
        $event->Nom=$Request->Nom;
        $event->Cognom=$Request->Cognom;
        $event->Curs=$Request->Curs;
        $event->Mail=$Request->Mail;
        if($Request->Practiques=="on")
            $event->Practiques = true;
        else
            $event->Practiques = false;
        $event->Telefon=$Request->Telefon;
        //$event->CV=$Request->CV->getClientOriginalExtension();
        $event->CV=$Request->CV;
        $event->DNI=$Request->DNI;
        $event->IDUser=$Request->IDUser;
        $event->save();

        return redirect('/alumnes')->with('success','Alumne creat amb èxit!.');
    }
}
