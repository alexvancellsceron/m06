<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('auth');

Auth::routes();

Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/empreses', [\App\Http\Controllers\HomeController::class, 'getEmpreses'])->name('empreses')->middleware('auth');
Route::get('/alumnes', [\App\Http\Controllers\HomeController::class, 'getAlumnes'])->name('alumnes')->middleware('auth');
Route::get('/ofertes', [\App\Http\Controllers\HomeController::class, 'getOfertes'])->name('ofertes')->middleware('auth');
Route::get('/empreses/add', [\App\Http\Controllers\CoordinadorFCTController::class, 'getFormulariEmpreses'])->name('add_empresa')->middleware('auth');
Route::get('/empreses/edit/{id}', [\App\Http\Controllers\CoordinadorFCTController::class, 'getFormulariEmpresesEditar'])->name('edit_empresa')->middleware('auth');
Route::get('/empreses/oferta/{idEmpresa}', [\App\Http\Controllers\CoordinadorFCTController::class, 'getFormulariOfertes'])->name('view_oferta')->middleware('auth');
Route::get('/empreses/oferta/edit/{idOferta}', [\App\Http\Controllers\CoordinadorFCTController::class, 'getFormulariOfertesEditar'])->name('edit_oferta')->middleware('auth');
Route::get('/empreses/oferta/add/{idEmpresa}', [\App\Http\Controllers\CoordinadorFCTController::class, 'getFormulariOfertesAfegir'])->name('add_oferta')->middleware('auth');

Route::get('/alumne/add', [\App\Http\Controllers\TutorPractiquesController::class, 'getFormulariAlumnesAfegir'])->name('add_alumne')->middleware('auth');
Route::get('/alumne/edit/{idAlumne}', [\App\Http\Controllers\TutorPractiquesController::class, 'getFormulariAlumnesEditar'])->name('edit_alumne')->middleware('auth');

Route::post('/newEmpresa', [\App\Http\Controllers\CoordinadorFCTController::class,'addEmpresa']);
Route::post('/editEmpresa', [\App\Http\Controllers\CoordinadorFCTController::class,'editEmpresa']);
Route::post('/actualitzaOferta', [\App\Http\Controllers\CoordinadorFCTController::class,'actualitzaOferta']);
Route::post('/creaOferta', [\App\Http\Controllers\CoordinadorFCTController::class,'insertOferta']);

Route::post('/insertAlumne', [\App\Http\Controllers\TutorPractiquesController::class,'insertAlumne']);
Route::post('/editAlumne', [\App\Http\Controllers\TutorPractiquesController::class,'editAlumne']);




Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('dashboard');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::patch('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::patch('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('{page}', ['as' => 'page.index', 'uses' => 'App\Http\Controllers\PageController@index']);
});

