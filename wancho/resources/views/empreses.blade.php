<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>

            </div>

        @endif
        <div class="card">
            <div class="card-header">{{ __('EMPRESES') }}</div>


            <div class="card-body">

                <table class="table table-striped table-hover mb-5">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nom</th>
                        <th>Adreça</th>
                        <th>Telefon</th>
                        <th>Mail</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($empreses as $empresa)
                        <tr id="{{$empresa->IDEmpresa}}">
                            <td> {{$empresa->IDEmpresa}} </td>
                            <td> {{$empresa->Nom}} </td>
                            <td> {{$empresa->Adreça}} </td>
                            <td> {{$empresa->Telefon}} </td>
                            <td> {{$empresa->Mail}} </td>
                            <td>
                                @if($user->Rol == "Coordinador De FCT")
                                <a title="Editar Empresa" class="btn btn-primary" href="{{route('edit_empresa', $empresa->IDEmpresa)}}">
                                    Editar
                                </a>
                                @endif
                            </td>
                            <td>
                                <a title="Afegir oferta" class="btn btn-primary" href="{{route('view_oferta', $empresa->IDEmpresa)}}">
                                    Ofertes de l'empresa
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if($user->Rol == "Coordinador De FCT")
                <a title="Afegir Empresa" class="btn btn-primary" href="empreses/add">
                    Nova Empresa
                </a>
                @endif
                {{-- Pagination --}}
                <div class="d-flex justify-content-center">
                    {{ $empreses->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection

</body>
</html>
