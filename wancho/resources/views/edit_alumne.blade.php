<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>

            </div>

        @endif
        <div class="card">
            <div class="card-header">{{ __('EDITAR EMPRESA') }}</div>


            <div class="card-body">

                <form action = "/editAlumne" method = "post">
                    <table>
                        <input type="hidden" name="id" value="{{$alumne->IDAlumnes}}"/>
                        <tr>
                            <td>Nom Alumne</td>
                            <td><input type='text' value="{{$alumne->Nom}}" name='Nom' class="form-control"/></td>
                        <tr>
                            <td>Cognom Alumne</td>
                            <td><input type="text" value="{{$alumne->Cognom}}" name='Cognom' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>DNI</td>
                            <td><input type="text" value="{{$alumne->DNI}}" name='DNI' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>Curs</td>
                            <td>
                                <select name="Curs" class="form-select" aria-label="Default select example">
                                    @foreach($cicles as $cicle)
                                        @if($cicle->NomCicle == $alumne->Curs)
                                            <option selected>{{$cicle->NomCicle}}</option>
                                        @else
                                            <option>{{$cicle->NomCicle}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Mail</td>
                            <td><input type="text" value="{{$alumne->Mail}}" name='Mail' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>Pràctiques</td>
                            @if($alumne->Practiques==1)
                            <td><input type="checkbox" checked name='Practiques' class="form-check-input"/></td>
                            @else
                            <td><input type="checkbox" name='Practiques' class="form-check-input"/></td>
                            @endif

                        </tr>
                        <tr>
                            <td>CV</td>
                            <td><input type="text" value="{{$alumne->CV}}" name='CV' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>Telèfon</td>
                            <td><input type="text" value="{{$alumne->Telefon}}" name='Telefon' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>Tutor</td>
                            <td><select name="IDUser" class="form-select" aria-label="Default select example">
                                    @foreach($tutors as $tutor)
                                        @if($tutor->IDUser == $alumne->IDUser)
                                            <option selected value="{{$tutor->IDUser}}">{{$tutor->name}}</option>
                                        @else
                                        <option value="{{$tutor->IDUser}}">{{$tutor->name}}</option>
                                        @endif
                                    @endforeach
                                </select></td>
                        </tr>

                        <tr>
                            <td colspan = '2'>
                                <hr>
                                <input type = 'submit' value ="Actualitza l'alumne"/>
                                {{ csrf_field() }}
                            </td>

                        </tr>
                    </table>
                </form>

            </div>
        </div>
    </div>
@endsection

</body>
</html>



