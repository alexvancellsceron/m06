<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>

            </div>

        @endif
        <div class="card">
            <div class="card-header">{{ __('AFEGIR ALUMNE') }}</div>


            <div class="card-body">

                <form action = "/insertAlumne" method = "post">
                    <table>
                        <tr>
                            <td>Nom Alumne</td>
                            <td><input type='text' name='Nom' class="form-control"/></td>
                        <tr>
                            <td>Cognom Alumne</td>
                            <td><input type="text" name='Cognom' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>DNI</td>
                            <td><input type="text" name='DNI' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>Curs</td>
                            <td>
                                <select name="Curs" class="form-select" aria-label="Default select example">
                                    @foreach($cicles as $cicle)
                                        <option>{{$cicle->NomCicle}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Mail</td>
                            <td><input type="text" name='Mail' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>Pràctiques</td>
                            <td><input type="checkbox" name='Practiques' class="form-check-input"/></td>
                        </tr>
                        <tr>
                            <td>CV</td>
                            <td><input type="file" name='CV' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>Telèfon</td>
                            <td><input type="text" name='Telefon' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>Tutor</td>
                            <td><select name="IDUser" class="form-select" aria-label="Default select example">
                                    @foreach($tutors as $tutor)
                                        <option value="{{$tutor->IDUser}}">{{$tutor->name}}</option>
                                    @endforeach
                                </select></td>
                        </tr>

                        <tr>
                            <td colspan = '2'>
                                <hr>
                                <input type = 'submit' value = "Afageix l'Alumne"/>
                                {{ csrf_field() }}
                            </td>

                        </tr>
                    </table>
                </form>

            </div>
        </div>
    </div>
@endsection

</body>
</html>



