<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>

            </div>

        @endif
        <div class="card">
            <div class="card-header">
                <td>{{($empresa->Nom)}}</td>
            </div>


            <div class="card-body">

                <table class="table table-striped table-hover mb-5">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Descripcio</th>
                        <th>Vacants</th>
                        <th>NomContacte</th>
                        <th>CognomContacte</th>
                        <th>MailContacte</th>
                        <th>Curs</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ofertes as $oferta)
                        <tr id="{{$oferta->IDOferta}}">
                            <td> {{$oferta->IDOferta}} </td>
                            <td> {{$oferta->Descripcio}} </td>
                            <td> {{$oferta->Vacants}} </td>
                            <td> {{$oferta->NomContacte}} </td>
                            <td> {{$oferta->CognomContacte}} </td>
                            <td> {{$oferta->MailContacte}} </td>
                            <td> {{$oferta->Curs}} </td>
                            <td>
                                @if($user->Rol == "Coordinador De FCT")
                                <a title="Editar Oferta" class="btn btn-primary" href="{{route('edit_oferta', $oferta->IDOferta)}}">
                                    Editar
                                </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @if($user->Rol == "Coordinador De FCT")
                <a title="Afegir Oferta" class="btn btn-primary" href="{{route('add_oferta', $empresa->IDEmpresa)}}">
                    Nova Oferta
                </a>
                @endif
            </div>
        </div>
    </div>
@endsection

</body>
</html>
