<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>

            </div>

        @endif
        <div class="card">
            <div class="card-header">{{ __('EDITAR OFERTA') }}</div>


            <div class="card-body">

                <form action = "/actualitzaOferta" method = "post">
                    <table>
                        <input type="hidden" name="id" value="{{$oferta->IDOferta}}"/>
                        <tr>
                            <td class="fw-bold">Descripció oferta</td>
                            <td><input type='text' name='descripcio' class="form-control" value="{{$oferta->Descripcio}}"/></td>
                        <tr>
                            <td class="fw-bold">Vacants oferta</td>
                            <td><input type="text" name='vacants' class="form-control" value="{{$oferta->Vacants}}"/></td>
                        </tr>
                        <tr>
                            <td class="fw-bold">Nom de contacte</td>
                            <td><input type="text" name='nomcontacte' class="form-control" value="{{$oferta->NomContacte}}"/></td>
                        </tr>
                        <tr>
                            <td class="fw-bold">Cognom de contacte</td>
                            <td><input type="text" name='cognomcontacte' class="form-control" value="{{$oferta->CognomContacte}}"/></td>
                        </tr>
                        <tr>
                            <td class="fw-bold">Mail de contacte</td>
                            <td><input type="text" name='mail' class="form-control" value="{{$oferta->MailContacte}}"/></td>
                        </tr>
                        <tr>
                            <td class="fw-bold">Curs destinatari</td>
                            <td>
                                <select class="form-select" name="curs" aria-label="Default select example">
                                    <option  selected>{{$oferta->Curs}}</option>
                                    @foreach($cicles as $cicle)
                                        @if($cicle->NomCicle!=$oferta->Curs)
                                        <option>{{$cicle->NomCicle}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td colspan = '2'>
                                <hr>
                                <input type = 'submit' value = "Actualitza l'Oferta"/>
                                {{ csrf_field() }}
                            </td>

                        </tr>
                    </table>
                </form>

            </div>
        </div>
    </div>
@endsection

</body>
</html>



