<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>

            </div>

        @endif
        <div class="card">
            <div class="card-header">{{ __('AFEGIR EMPRESA') }}</div>


            <div class="card-body">

                <form action = "/newEmpresa" method = "post">
                    <table>
                        <tr>
                            <td>Nom Empresa</td>
                            <td><input type='text' name='nom' class="form-control"/></td>
                        <tr>
                            <td>Adreça</td>
                            <td><input type="text" name='adreca' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>Telèfon</td>
                            <td><input type="text" name='telefon' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td>Mail</td>
                            <td><input type="text" name='mail' class="form-control"/></td>
                        </tr>

                        <tr>
                            <td colspan = '2'>
                                <hr>
                                <input type = 'submit' value = "Afageix l'Empresa"/>
                                {{ csrf_field() }}
                            </td>

                        </tr>
                    </table>
                </form>

            </div>
        </div>
    </div>
@endsection

</body>
</html>



