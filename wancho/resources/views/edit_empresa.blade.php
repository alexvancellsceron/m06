<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>

            </div>

        @endif
        <div class="card">
            <div class="card-header">{{ __('EDITAR EMPRESA') }}</div>


            <div class="card-body">

                <form action = "/editEmpresa" method = "post">
                    <table>
                        <input type="hidden" name="id" value="{{$empresa->IDEmpresa}}"/>
                        <tr>
                            <td class="fw-bold">Nom empresa</td>
                            <td><input type='text' name='nom' class="form-control" value="{{$empresa->Nom}}"/></td>
                        <tr>
                            <td class="fw-bold">Adreça empresa</td>
                            <td><input type="text" name='adreca' class="form-control" value="{{$empresa->Adreça}}"/></td>
                        </tr>
                        <tr>
                            <td class="fw-bold">Telèfon empresa</td>
                            <td><input type="text" name='telefon' class="form-control" value="{{$empresa->Telefon}}"/></td>
                        </tr>
                        <tr>
                            <td class="fw-bold">Mail empresa</td>
                            <td><input type="text" name='mail' class="form-control" value="{{$empresa->Mail}}"/></td>
                        </tr>

                        <tr>
                            <td colspan = '2'>
                                <hr>
                                <input type = 'submit' value = "Actualitza l'Empresa"/>
                                {{ csrf_field() }}
                            </td>

                        </tr>
                    </table>
                </form>

            </div>
        </div>
    </div>
@endsection

</body>
</html>



