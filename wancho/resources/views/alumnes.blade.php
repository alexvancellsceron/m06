<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>

            </div>

        @endif
        <div class="card">
            <div class="card-header">{{ __('alumnes') }}</div>


            <div class="card-body">

                <table class="table table-striped table-hover mb-5">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nom</th>
                        <th>Cognom</th>
                        <th>DNI</th>
                        <th>Curs</th>
                        <th>Telefon</th>
                        <th>Mail</th>
                        <th>Practiques</th>
                        <th>CV</th>
                        <th>Tutor</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($alumnes as $alumne)
                        @if(($user->Rol == "Tutor De practiques" && $alumne->Curs == $user->Grup) || $user->Rol == "Coordinador De FCT")
                        <tr id="{{$alumne->IDAlumnes}}">
                            <td> {{$alumne->IDAlumnes}} </td>
                            <td> {{$alumne->Nom}} </td>
                            <td> {{$alumne->Cognom}} </td>
                            <td> {{$alumne->DNI}} </td>
                            <td> {{$alumne->Curs}} </td>
                            <td> {{$alumne->Telefon}} </td>
                            <td> {{$alumne->Mail}} </td>
                            <td> {{$alumne->Practiques}} </td>
                            <td> {{$alumne->CV}} </td>
                            @foreach($tutors as $tutor)
                                @if($tutor->IDUser == $alumne->IDUser)
                                    <td> {{$tutor->name}} </td>
                                @endif
                            @endforeach
                            <td>
                                <a title="Editar Alumne" class="btn btn-primary" href="{{route("edit_alumne", $alumne->IDAlumnes)}}">

                                    Editar

                                </a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
                <a title="Afegir Alumne" class="btn btn-primary" href="/alumne/add">

                    Nou Alumne

                </a>
                {{-- Pagination --}}
                <div class="d-flex justify-content-center">
                    {{ $alumnes->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection

</body>
</html>
