<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
@extends('layouts.app')
@section('content')
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                <strong>{{ $message }}</strong>

            </div>

        @endif
        <div class="card">
            <div class="card-header">{{ __('AFEGIR OFERTA') }}</div>


            <div class="card-body">

                <form action = "/creaOferta" method = "post">
                    <table>
                        <input type="hidden" name="id" value="{{$empresa->IDEmpresa}}"/>
                        <tr>
                            <td class="fw-bold">Descripció oferta</td>
                            <td><input type='text' name='descripcio' class="form-control"/></td>
                        <tr>
                            <td class="fw-bold">Vacants oferta</td>
                            <td><input type="text" name='vacants' class="form-control" value="0"/></td>
                        </tr>
                        <tr>
                            <td class="fw-bold">Nom de contacte</td>
                            <td><input type="text" name='nomcontacte' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="fw-bold">Cognom de contacte</td>
                            <td><input type="text" name='cognomcontacte' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="fw-bold">Mail de contacte</td>
                            <td><input type="text" name='mail' class="form-control"/></td>
                        </tr>
                        <tr>
                            <td class="fw-bold">Curs destinatari</td>
                            <td>
                                <select name="curs" class="form-select" aria-label="Default select example">
                                    @foreach($cicles as $cicle)
                                    <option>{{$cicle->NomCicle}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td colspan = '2'>
                                <hr>
                                <input type = 'submit' value = "Crea l'Oferta"/>
                                {{ csrf_field() }}
                            </td>

                        </tr>
                    </table>
                </form>

            </div>
        </div>
    </div>
@endsection

</body>
</html>



