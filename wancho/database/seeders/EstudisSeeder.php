<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstudisSeeder extends Seeder
{
    //'SMX', 'DAMVI', 'DAM', 'ASIX'
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Estudis')->insert([
            'IDEstudi' => 1,
            'NomCicle' => 'DAMVI',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('Estudis')->insert([
            'IDEstudi' => 2,
            'NomCicle' => 'SMX',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('Estudis')->insert([
            'IDEstudi' => 3,
            'NomCicle' => 'DAM',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('Estudis')->insert([
            'IDEstudi' => 4,
            'NomCicle' => 'ASIX',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
