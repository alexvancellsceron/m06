<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Users')->insert([
            'IDUser' => 1,
            'name' => 'David Vilellas',
            'email' => 'velas@carpediem.net',
            'password' => Hash::make('super3'),
            'Rol' => 'Tutor De practiques',
            'Grup' => 'ASIX',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('Users')->insert([
            'IDUser' => 2,
            'name' => 'Hector Mudarra',
            'email' => 'hectorm@carpediem.net',
            'password' => Hash::make('super3'),
            'Rol' => 'Tutor De practiques',
            'Grup' => 'DAMVI',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('Users')->insert([
            'IDUser' => 3,
            'name' => 'Nicolas Torrubio',
            'email' => 'milbicis@carpediem.net',
            'password' => Hash::make('super3'),
            'Rol' => 'Coordinador De FCT',
            'Grup' => 'admin',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('Users')->insert([
            'IDUser' => 4,
            'name' => 'Carlos el Guapo',
            'email' => 'vegano@carpediem.net',
            'password' => Hash::make('super3'),
            'Rol' => 'Tutor De practiques',
            'Grup' => 'SMX',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('Users')->insert([
            'IDUser' => 5,
            'name' => 'Eloi Vazquez',
            'email' => 'cyborg@carpediem.net',
            'password' => Hash::make('super3'),
            'Rol' => 'Tutor De practiques',
            'Grup' => 'DAM',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
