<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class EmpresesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Empreses')->insert([
            'IDEmpresa' => 1,
            'Nom' => 'MGA',
            'Adreça' => 'Cerdanyola, 99',
            'Telefon' => 654987311,
            'Mail' => 'mga@mga.es',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('Empreses')->insert([
            'IDEmpresa' => 2,
            'Nom' => 'IDP',
            'Adreça' => 'sabadell, 69',
            'Telefon' => 654329871,
            'Mail' => 'idp@idp.es',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
