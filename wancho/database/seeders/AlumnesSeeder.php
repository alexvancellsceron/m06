<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlumnesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Alumnes')->insert([
            'IDAlumnes' => 1,
            'Nom' => 'Alma',
            'Cognom' => 'Morante',
            'DNI' => '46578912A',
            'Curs' => 'DAMVI',
            'Mail' => 'amorantem@ies-sabadell.cat',
            'Practiques' => true,
            'CV' => 'linkcvalma.com',
            'Telefon' => 654987321,
            'IDUser' => 2,
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('Alumnes')->insert([
            'IDAlumnes' => 2,
            'Nom' => 'Alex',
            'Cognom' => 'Vancells',
            'DNI' => '46778912A',
            'Curs' => 'DAMVI',
            'Mail' => 'avancellsc@ies-sabadell.cat',
            'Practiques' => true,
            'CV' => 'linkcvalex.com',
            'Telefon' => 666987321,
            'IDUser' => 2,
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('Alumnes')->insert([
            'IDAlumnes' => 3,
            'Nom' => 'Abraham',
            'Cognom' => 'TieneUnoSeguro',
            'DNI' => '45789612A',
            'Curs' => 'SMX',
            'Mail' => 'abraham@ies.cat',
            'Practiques' => true,
            'CV' => 'linkcvaabraham.com',
            'Telefon' => 654888123,
            'IDUser' => 4,
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('Alumnes')->insert([
            'IDAlumnes' => 4,
            'Nom' => 'Izan',
            'Cognom' => 'Mata?',
            'DNI' => '45587921A',
            'Curs' => 'DAM',
            'Mail' => 'izan@ies.cat',
            'Practiques' => false,
            'CV' => 'linkcvaizan.com',
            'Telefon' => 645999111,
            'IDUser' => 5,
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
