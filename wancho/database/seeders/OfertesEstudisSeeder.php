<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OfertesEstudisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ofertes_estudis')->insert([
            'IDEstudi_IDOferta' => 1,
            'IDEstudi' => 1,
            'IDOferta' => 1,
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('ofertes_estudis')->insert([
            'IDEstudi_IDOferta' => 2,
            'IDEstudi' => 2,
            'IDOferta' => 2,
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('ofertes_estudis')->insert([
            'IDEstudi_IDOferta' => 3,
            'IDEstudi' => 3,
            'IDOferta' => 3,
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('ofertes_estudis')->insert([
            'IDEstudi_IDOferta' => 4,
            'IDEstudi' => 1,
            'IDOferta' => 4,
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
