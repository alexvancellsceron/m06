<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OfertesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Ofertes')->insert([
            'IDOferta' => 1,
            'Descripcio' => 'Volem trolejar als alumnes.',
            'Vacants' => 2,
            'NomContacte' => 'Raúl',
            'CognomContacte' => 'de MGA',
            'MailContacte' => 'raul@mga.es',
            'Curs' => 'DAMVI',
            'IDEmpresa' => '1',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('Ofertes')->insert([
            'IDOferta' => 2,
            'Descripcio' => 'Volem als millors alumnes.',
            'Vacants' => 2,
            'NomContacte' => 'Raúl',
            'CognomContacte' => 'de MGA',
            'MailContacte' => 'raul@mga.es',
            'Curs' => 'SMX',
            'IDEmpresa' => '1',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('Ofertes')->insert([
            'IDOferta' => 3,
            'Descripcio' => 'Volem als millors alumnes.',
            'Vacants' => 1,
            'NomContacte' => 'Raúl',
            'CognomContacte' => 'de IDP',
            'MailContacte' => 'raul@idp.es',
            'Curs' => 'DAM',
            'IDEmpresa' => '2',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('Ofertes')->insert([
            'IDOferta' => 4,
            'Descripcio' => "Necessitem a l'Alma.",
            'Vacants' => 1,
            'NomContacte' => 'Raúl',
            'CognomContacte' => 'de IDP',
            'MailContacte' => 'raul@idp.es',
            'Curs' => 'DAMVI',
            'IDEmpresa' => '2',
            'created_at'=> Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
