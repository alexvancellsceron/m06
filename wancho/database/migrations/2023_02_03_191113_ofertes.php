<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Ofertes', function (Blueprint $table) {
            $table->bigIncrements("IDOferta");
            $table->string("Descripcio",30);
            $table->integer("Vacants");
            $table->string("NomContacte",9);
            $table->string("CognomContacte",30);
            $table->string("MailContacte",30);
            $table->string("Curs",30);
            $table->foreignId('IDEmpresa')->nullable()->constrained('Empreses')->references('IDEmpresa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Ofertes');
    }
};
