<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('ofertes_estudis', function (Blueprint $table) {
            $table->bigIncrements("IDEstudi_IDOferta");
            $table->foreignId('IDEstudi')->nullable()->constrained('Estudis')->references('IDEstudi');
            $table->foreignId('IDOferta')->nullable()->constrained('Ofertes')->references('IDOferta');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ofertes_estudis');
    }
};
