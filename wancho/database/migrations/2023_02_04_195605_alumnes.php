<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Alumnes', function (Blueprint $table) {
            $table->bigIncrements("IDAlumnes");
            $table->string("Nom",30);
            $table->string("Cognom",30);
            $table->string("DNI",9);
            $table->string("Curs",30);
            $table->string("Mail",30);
            $table->boolean("Practiques")->default(false);
            $table->string("CV",300);
            $table->integer("Telefon");
            $table->foreignId('IDUser')->nullable()->constrained('Users')->references('IDUser');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Alumnes');
    }
};
