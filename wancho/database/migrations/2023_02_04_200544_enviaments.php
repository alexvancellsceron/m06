<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Enviaments', function (Blueprint $table) {
            $table->bigIncrements("IDAlumnes_IDOferta");
            $table->string('Observacions');
            $table->enum('EstatEnviament', ['NoConveni', 'Acceptat', 'FinalitzatIContractat', 'FinalitzatINoContractat', 'Expulsat']);
            $table->foreignId('IDAlumnes')->nullable()->constrained('Alumnes')->references('IDAlumnes');
            $table->foreignId('IDOferta')->nullable()->constrained('Ofertes')->references('IDOferta');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Esdeveniments');
    }
};
