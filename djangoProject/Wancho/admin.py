from django.contrib import admin
from django.contrib.admin import ModelAdmin
from Wancho.models import *


class VideojocAdmin(ModelAdmin):
    pass

class PlataformaAdmin(ModelAdmin):
    pass

class UsuariPlataformaAdmin(ModelAdmin):
    pass

admin.site.register(Videojoc, ModelAdmin)
admin.site.register(Plataforma, ModelAdmin)
admin.site.register(usuari_plataforma, ModelAdmin)
# Register your models here.
