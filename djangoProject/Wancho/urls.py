from django.urls import path, include
from . import views
from Wancho.views import *

app_name = 'Wancho'

urlpatterns = [
    path('', Wancho.as_view()),
    path('addVideojocs/', addVideojocView.as_view(), name="addVideojocs"),
    path('addPlataformes/', addPlataformaView.as_view(), name="addPlataformes"),
    path('plataformaRandom/', plataformaRandom.as_view(), name="plataformaRandom"),
    path('plataformaRandomNou/', plataformaRandomNou.as_view(),name="plataformaRandomNou"),
    path('plataformaUserView/', plataformaUserView.as_view(),name="plataformaUser"),
    path('videojocUserView/', videojocUserView.as_view(),name="videojocUser"),
    path('userPlatformsView/<id>', userPlatformsView.as_view(),name="userAllPlatforms"),
    path('userPlatformView/<id>', plataformaUserRandom.as_view(),name="userOnePlatform"),
    path('removeRelGameUser/<id>', removeRelGameUser.as_view(),name="remove")

]