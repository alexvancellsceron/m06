from django.http import HttpRequest, HttpResponse
from django.shortcuts import render, get_object_or_404,redirect
from django.http import Http404
from django.contrib.auth import logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import View
from Wancho.models import *
from datetime import datetime
import random


class Wancho(View):
    # Definim el mètode HTTP el qual s'ha d'atendre
    def get(self, request):
        context = {
            'videojocs': list(Videojoc.objects.all()),
            'plataformes': list(Plataforma.objects.all()),
            'usuaris': list(Usuari.objects.all()),
            'usuairsPlataformes': list(usuari_plataforma.objects.all()),
            'usuarisVideojocs': list(usuari_videojoc.objects.all())
        }
        return render(request, 'MainMenu.html', context=context)


class addVideojocView(View):
    # Definim el mètode HTTP el qual s'ha d'atendre
    def get(self, request):
        context = {
            'videojocs': list(Videojoc.objects.all()),
            'plataformes': list(Plataforma.objects.all())
        }
        return render(request, 'addVideojoc.html', context=context)

    def post(self, request):
        if request.method == 'POST':
            platf = Plataforma.objects.get(Nom=request.POST.get('Plataforma'))
            if request.POST.get('Nou') == None:
                Videojoc.objects.create(Nom=request.POST.get('Nom'), Preu=request.POST.get('Preu'),Nou=0, Plataforma=platf)
            else:
                Videojoc.objects.create(Nom=request.POST.get('Nom'), Preu=request.POST.get('Preu'), Nou=1, Plataforma=platf)
        return redirect('/')

class addPlataformaView(View):
    # Definim el mètode HTTP el qual s'ha d'atendre
    def get(self, request):
        context = {
            'plataformes': list(Plataforma.objects.all())
        }
        return render(request, 'addPlataforma.html', context=context)

    def post(self, request):
        if request.method == 'POST':
            Plataforma.objects.create(Nom=request.POST.get('Nom'), Data=datetime.today().strftime('%Y-%m-%d %H:%M:%S'))
        return redirect('/')

class plataformaRandom(View):
    def get(self, request):
        random_plataforma = random.choice(Plataforma.objects.all())
        return HttpResponse(content="Plataforma random: "+random_plataforma.Nom)

class plataformaRandomNou(View):
    def get(self, request):
        random_videojoc_nou = random.choice(Videojoc.objects.select_related('Plataforma').filter(Plataforma__videojoc__Nou=True))
        random_plataforma_nou = Plataforma.objects.filter(id=random_videojoc_nou.Plataforma.id)
        return HttpResponse(content="Plataforma random: " + str(random_plataforma_nou[0].Nom))

class plataformaUserView(View):
    def get(self, request):
        context = {
            'usuaris': list(Usuari.objects.all()),
            'plataformes': list(Plataforma.objects.all())
        }
        return render(request, 'associatePlatformUser.html', context=context)

    def post(self, request):
        if request.method == 'POST':
            u=Usuari.objects.get(Nom=request.POST.get('Usuari'))
            p=Plataforma.objects.get(Nom=request.POST.get('Plataforma'))
            usuari_plataforma.objects.create(Usuari=u, Plataforma=p)
        return redirect('/')

class videojocUserView(View):
    def get(self, request):
        context = {
            'usuaris': list(Usuari.objects.all()),
            'videojocs': list(Videojoc.objects.all())
        }
        return render(request, 'associateGameUser.html', context=context)

    def post(self, request):
        if request.method == 'POST':
            u=Usuari.objects.get(Nom=request.POST.get('Usuari'))
            v=Videojoc.objects.get(Nom=request.POST.get('Videojoc'))
            usuari_videojoc.objects.create(Usuari=u, Videojoc=v)
        return redirect('/')

class userPlatformsView(View):
    def get(self, request, id):
        up= list(usuari_plataforma.objects.filter(Usuari_id=id))
        p = list()
        for x in range(len(up)):
            p += list(Plataforma.objects.filter(id=up[x].Plataforma_id))

        context = {
            'plataformes': p
        }
        return render(request, 'userPlatformsView.html', context=context)

class plataformaUserRandom(View):
    def get(self, request, id):
        random_videojoc_nou = random.choice(usuari_plataforma.objects.select_related('Usuari').filter(Usuari_id=id))
        random_plataforma_nou = Plataforma.objects.filter(id=random_videojoc_nou.Plataforma.id)
        context = {
            'plataformes': random_plataforma_nou
        }
        return render(request, 'userPlatformsView.html', context=context)


class removeRelGameUser(View):
    def post(self, request, id):
        rel =  get_object_or_404(usuari_videojoc, id = id)
        rel.delete()
        return redirect('/')